package com.huixi.miniProgram.service.impl;
import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.miniProgram.mapper.WjUserMapper;
import com.huixi.miniProgram.pojo.entity.user.WjUser;
import com.huixi.miniProgram.pojo.entity.user.WjUserWx;
import com.huixi.miniProgram.service.WjUserService;
import com.huixi.miniProgram.service.WjUserWxService;
import com.huixi.miniProgram.util.CommonUtil;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class WjUserServiceImpl extends ServiceImpl<WjUserMapper, WjUser> implements WjUserService {

    @Resource
    private WxMaService wxMaService;

    @Resource
    private WjUserWxService wjUserWxService;

    /**
     * 获取session_key
     *
     * @param code 用户code
     * @return java.lang.String
     * @Author 李辉
     * @Date 2019/11/23 4:31
     **/
    @SneakyThrows
    @Override
    public WxMaJscode2SessionResult getWxSession(String code) {
        return wxMaService.getUserService().getSessionInfo(code);
    }


    /**
     * 根据用户code 获取用户的 openId 和 sessionKey , 来确定有没有授权过。 如果授权发放用户信息 ，没有就创建一个用户
     *
     * @param code 用户小程序的code
     * @return WjUser 用户信息
     * @Author 叶秋
     * @Date 2020/2/1 20:17
     **/
    @Override
    public WjUser detectionUserAuthorization(String code) {

        WxMaJscode2SessionResult wxSessionKey = getWxSession(code);

        // 根据open_id 检测数据库中是否有此用户
        WjUserWx wjUserWx = wjUserWxService.lambdaQuery().eq(WjUserWx::getWxOpenId, wxSessionKey.getOpenid()).one();

        if (ObjectUtil.isNotNull(wjUserWx)) {
            return lambdaQuery().eq(WjUser::getUserId, wjUserWx.getUserId()).one();
        }

        // 检测用户不存在的情况 随机创建用户资料
        WjUser wjUser = CommonUtil.randomCreateUserInfo();
        WjUserWx newWjUserWx = new WjUserWx().setUserId(wjUser.getUserId())
                .setWxOpenId(wxSessionKey.getOpenid());

        boolean save = save(wjUser);
        boolean save1 = wjUserWxService.save(newWjUserWx);

        return wjUser;

    }

}
