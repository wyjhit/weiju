package com.huixi.miniProgram.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.miniProgram.pojo.entity.user.WjUser;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjUserMapper extends BaseMapper<WjUser> {

}
