package com.huixi.miniProgram.controller;


import com.huixi.commonutils.util.wrapper.ResultData;
import com.huixi.miniProgram.client.OssClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 *  测试内容
 * @Author 叶秋
 * @Date 2021/4/11 13:18
 * @param
 * @return
 **/
@RestController
@Slf4j
public class MiniController {

    @Resource
    private OssClient ossClient;

    @GetMapping(value = "helloMini")
    public ResultData helloMini(){

        log.info("helloMini");

        return ResultData.ok("helloMini");
    }

    @GetMapping(value = "helloOss")
    public ResultData helloOss(){
        return ossClient.helloOss();
    }


}
