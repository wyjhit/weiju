package com.huixi.miniProgram.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.miniProgram.pojo.entity.user.WjUserWx;

/**
 * <p>
 * 专门用来存储微信后台发送给我们的数据 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjUserWxService extends IService<WjUserWx> {

}
