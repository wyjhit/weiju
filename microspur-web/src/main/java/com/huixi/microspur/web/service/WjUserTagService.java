package com.huixi.microspur.web.service;

import com.huixi.microspur.web.pojo.entity.user.WjUserTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-03
 */
public interface WjUserTagService extends IService<WjUserTag> {


    /**
     *  检测是否拥有此标签
     * @Author 叶秋
     * @Date 2020/7/7 21:41
     * @param userId 用户id
     * @param tagId 标签id
     * @return java.lang.Boolean
     **/
    Boolean detectionIsPossess(String userId, int tagId);


    /**
     *  设置标签
     * @Author 叶秋
     * @Date 2020/7/7 22:04
     * @param userId 用户id
     * @param tagId 标签id
     * @return java.lang.Boolean
     **/
    Boolean settingTag(String userId, Integer tagId);


}
