package com.huixi.microspur.web.pojo.entity.chat;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.servicebase.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 聊天室（聊天列表）
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_chat")
@ApiModel(value="WjChat对象", description="聊天室（聊天列表）")
public class WjChat extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "chat_id", type = IdType.ASSIGN_UUID)
    private String chatId;

    @TableId(value = "appeal_id")
    private String appealId;

    @ApiModelProperty(value = "聊天的类型（one:一对一，more:一对多）")
    @TableField("type")
    private String type;

    @ApiModelProperty(value = "标题")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "限制人数")
    @TableField("people_number")
    private Integer peopleNumber;

    @ApiModelProperty(value = "聊天室的图片（一对一聊天就用双方的头像组合成一张，一对多就多张组合起来）")
    @TableField("url")
    private String url;

    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;



}
