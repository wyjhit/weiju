package com.huixi.microspur.web.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.mapper.WjAppealAddressMapper;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealAddress;
import com.huixi.microspur.web.service.WjAppealAddressService;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.core.BoundGeoOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 诉求地址 服务实现类
 *
 * @param
 * @Author 叶秋
 * @Date 2020/3/25 2:23
 * @return
 **/
@Service
public class WjAppealAddressServiceImpl extends ServiceImpl<WjAppealAddressMapper, WjAppealAddress> implements WjAppealAddressService {


    @Resource
    private RedisTemplate redisTemplate;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean saveAppealAddress(WjAppealAddress wjAppealAddress) {
        boolean save = false;


        // 纬度 腾讯坐标系
        Double latitude = Double.valueOf(wjAppealAddress.getLatitude());
        // 经度 腾讯坐标系
        Double longitude = Double.valueOf(wjAppealAddress.getLongitude());

        BoundGeoOperations boundGeoOperations = redisTemplate.boundGeoOps("wjAppeal");
        // 位置信息要经度在前
        boundGeoOperations.add(new Point(longitude, latitude), wjAppealAddress.getAppealId());

        save = save(wjAppealAddress);

        return save;
    }


}
