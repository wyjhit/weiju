package com.huixi.microspur.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealEndorse;

/**
 *  诉求点赞记录表的 mapper 接口
 * @Author 叶秋
 * @Date 2020/3/15 15:19
 * @param
 * @return
 **/
public interface WjAppealEndorseMapper extends BaseMapper<WjAppealEndorse> {
}
