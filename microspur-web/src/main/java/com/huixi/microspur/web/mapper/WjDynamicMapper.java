package com.huixi.microspur.web.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamic;

/**
 * <p>
 * 动态表 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjDynamicMapper extends BaseMapper<WjDynamic> {

}
