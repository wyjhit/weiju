package com.huixi.microspur.web.pojo.dto.appeal;

import com.huixi.commonutils.page.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 前端分页查询诉求 要给的参数 （附近的是另外一个哦）
 * @Author 叶秋
 * @Date 2020/7/14 18:11
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "前端分页查询诉求 要给的参数 （附近的是另外一个哦）")
public class WjAppealPageDTO {

    @ApiModelProperty(value = "用户id", hidden = true)
    private String userId;

    @ApiModelProperty(value = "模糊搜索内容")
    private String title;

    @ApiModelProperty(value = "是否根据创建时间排序, 是否根据这个排序")
    private Boolean createTime;

    @ApiModelProperty(value = "是否根据点赞的次数排序, 是否根据这个排序")
    private Boolean endorseCount;


    @ApiModelProperty(value = "分页参数")
    private PageQuery pageQuery;

}
