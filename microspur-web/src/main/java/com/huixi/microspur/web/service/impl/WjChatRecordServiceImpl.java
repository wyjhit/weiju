package com.huixi.microspur.web.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.commonutils.page.PageData;
import com.huixi.commonutils.page.PageFactory;
import com.huixi.microspur.web.pojo.vo.chat.WjChatRecordPageVO;
import com.huixi.microspur.web.pojo.entity.chat.WjChatRecord;
import com.huixi.microspur.web.mapper.WjChatRecordMapper;
import com.huixi.microspur.web.service.WjChatRecordService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 聊天室-聊天记录 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjChatRecordServiceImpl extends ServiceImpl<WjChatRecordMapper, WjChatRecord> implements WjChatRecordService {

    @Override
    public PageData queryPageWjChatRecord(WjChatRecordPageVO wjChatRecordPageVO) {

        PageData pageData = new PageData();

        Page<WjChatRecord> page = PageFactory.createPage(wjChatRecordPageVO.getPageQuery());

        QueryWrapper<WjChatRecord> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("chat_id", wjChatRecordPageVO.getChatId());
        objectQueryWrapper.orderByDesc("create_time");

        Page<WjChatRecord> page1 = page(page, objectQueryWrapper);

        BeanUtil.copyProperties(page1, pageData);

        return pageData;
    }

    @Async
    @Override
    public void asynSaveChatRecord(WjChatRecord wjChatRecord) {
        save(wjChatRecord);
    }
}
