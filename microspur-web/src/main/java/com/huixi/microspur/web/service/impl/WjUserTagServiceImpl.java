package com.huixi.microspur.web.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.huixi.commonutils.enums.UserTagEnum;
import com.huixi.microspur.web.pojo.entity.user.WjUserTag;
import com.huixi.microspur.web.mapper.WjUserTagMapper;
import com.huixi.microspur.web.service.WjUserTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-03
 */
@Service
public class WjUserTagServiceImpl extends ServiceImpl<WjUserTagMapper, WjUserTag> implements WjUserTagService {

    @Override
    public Boolean detectionIsPossess(String userId, int tagId) {

        QueryWrapper<WjUserTag> wjUserTagQueryWrapper = new QueryWrapper<>();
        wjUserTagQueryWrapper.eq("user_id", userId)
                .eq("tag_id", tagId);

        WjUserTag one = getOne(wjUserTagQueryWrapper);

        if(ObjectUtil.isNotNull(one)){
            return true;
        }

        return false;
    }

    @Override
    public Boolean settingTag(String userId, Integer tagId) {

        UserTagEnum userTagEnum = UserTagEnum.byIdTagValue(tagId);

        // 关闭掉自己所有的标签
        UpdateWrapper<WjUserTag> wjUserTagUpdateWrapper = new UpdateWrapper<>();
        wjUserTagUpdateWrapper.eq("user_id", userId).set("check", false);
        boolean update = update(wjUserTagUpdateWrapper);

        // 设置标签
        UpdateWrapper<WjUserTag> wjUserTagUpdateWrapper1 = new UpdateWrapper<>();
        wjUserTagUpdateWrapper1.eq("user_id", userId).eq("tag_id", tagId)
                .set("check", true);

        boolean update1 = update(wjUserTagUpdateWrapper1);

        return update1;
    }
}
