package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.pojo.entity.user.WjUserWx;

/**
 * <p>
 * 专门用来存储微信后台发送给我们的数据 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjUserWxService extends IService<WjUserWx> {

}
