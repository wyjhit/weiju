package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamicEndorse;
import com.huixi.microspur.web.mapper.WjDynamicEndorseMapper;
import com.huixi.microspur.web.service.WjDynamicEndorseService;
import org.springframework.stereotype.Service;

/**
 *  动态点赞 实现类
 * @Author 叶秋 
 * @Date 2020/4/20 22:13
 * @param 
 * @return 
 **/
@Service
public class WjDynamicEndorseServiceImpl extends ServiceImpl<WjDynamicEndorseMapper, WjDynamicEndorse> implements WjDynamicEndorseService {


    /**
     * 判断用户是否点赞
     *
     * @param dynamicId 动态id
     * @param userId    用户id
     * @return java.lang.Boolean
     * @Author 叶秋
     * @Date 2020/3/19 0:15
     **/
    @Override
    public Boolean isEndorse(String dynamicId, String userId) {

        QueryWrapper<WjDynamicEndorse> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("dynamic_id", dynamicId).eq("user_id", userId);
        int count = count(objectQueryWrapper);
        if(count!=0){
            return true;
        }

        return false;

    }



    /**
     * 获取改诉求的总点赞数
     *
     * @param dynamicId 动态id
     * @return int
     * @Author 叶秋
     * @Date 2020/3/19 0:25
     **/
    @Override
    public int getTotleCount(String dynamicId) {

        QueryWrapper<WjDynamicEndorse> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("dynamic_id", dynamicId);

        return count(objectQueryWrapper);
    }




    /**
     * 取消诉求点赞
     *
     * @param wjAppealEndorse
     * @return java.lang.Boolean
     * @Author 叶秋
     * @Date 2020/3/20 0:37
     **/
    @Override
    public Boolean cancelEndorse(WjDynamicEndorse wjAppealEndorse) {

        QueryWrapper<WjDynamicEndorse> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("dynamic_id", wjAppealEndorse.getDynamicId()).eq("user_id", wjAppealEndorse.getUserId());

        boolean remove = remove(objectQueryWrapper);

        return remove;
    }


}
