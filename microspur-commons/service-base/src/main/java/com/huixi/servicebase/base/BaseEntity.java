package com.huixi.servicebase.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.huixi.commonutils.constant.Constant;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 基础实体类，所有实体都需要继承
 * @author 叶秋
 */
@Data
public abstract class BaseEntity implements Serializable {



    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    public LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     *  逻辑删除
     */
    @TableLogic(value = Constant.FALSE, delval = Constant.TRUE)
    @TableField(value = "del_flag",fill = FieldFill.INSERT)
    private Boolean delFlag;

    /**
     *  是否启用
     */
    @TableField(value = "enable_flag",fill = FieldFill.INSERT)
    private Integer enableFlag;



}
